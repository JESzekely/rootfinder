CC = /opt/local/bin/g++-mp-4.9

CFLAGS = -fdiagnostics-color=auto -O3 -I$(MKLROOT)/include -Wall -Wno-sign-compare -Wno-unused-function -Werror -std=c++11 -openmp

BOOST_INC = -I/opt/local/include/

OBJ  =   obj/main.o  obj/polynomial.o

LIBS =  -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core -lmkl_sequential -lpthread -lm

HEADS =  src/main.hpp src/polynomial.hpp
BIN  =   RootFinder

RM = rm -f

.PHONY: all all-before all-after clean clean-custom
all: all-before $(BIN) all-after

clean: clean-custom
	${RM} $(OBJ) $(BIN)

$(BIN): $(OBJ)
	$(CC) $(CFLAGS) -o $(BIN) -I./src $(OBJ) $(BOOST_INC) $(LIBS)

obj/main.o: src/main.cpp
	$(CC) $(CFLAGS) -c  src/main.cpp -o obj/main.o $(BOOST_INC) -I./src

obj/polynomial.o: src/polynomial.cpp
	$(CC) $(CFLAGS)  -c src/polynomial.cpp  -o obj/polynomial.o  -I./src
