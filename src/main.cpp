#include <iostream>
#include <algorithm>
#include <memory>
#include "polynomial.hpp"
#include <random>

using namespace std;

int main(int argc, char const *argv[])
{
  shared_ptr<polynomial<cplx>> H = Legendre<cplx>(30);
  polynomial<cplx> Hprime = H->derivative();
  H->findRoots();
  Hprime.findRoots();
  return(0);
}